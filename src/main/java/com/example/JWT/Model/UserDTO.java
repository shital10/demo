package com.example.JWT.Model;

import lombok.Data;

@Data
public class UserDTO {
    private Long id;
    private String username;
    private String password;

}
